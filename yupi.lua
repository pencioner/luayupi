#!/usr/bin/env lua

USAGE = ([[
<some command output> | %s <options> { | less -R }
for example:
tail -f /var/log/everything.log | %s

]]):format(arg[0], arg[0])


ESC = string.char(27)

COLOR_CODES = {
  black   = "0",
  red     = "1",
  green   = "2",
  yellow  = "3",
  blue    = "4",
  magenta = "5",
  cyan    = "6",
  white   = "7",
  gray    = "7"
}

function format_codes(fmt)
  local rv = {}
  for k,v in pairs(COLOR_CODES) do rv[k] = fmt:format(v) end
  return rv
end

RESET_COLORS = ESC.."[0m"
FOREGROUND_FMT = ESC.."[3%sm"
BACKGROUNG_FMT = ESC.."[4%sm"
FG_COLORS, BG_COLORS = format_codes(FOREGROUND_FMT), format_codes(BACKGROUNG_FMT)
FG_COLORS.default = RESET_COLORS
BG_COLORS.default = RESET_COLORS
FG_COLORS[""] = FG_COLORS.default
BG_COLORS[""] = BG_COLORS.default


require "rex_pcre"

function colorize_line(line_data, patterns)
  for __, _pattern in ipairs(patterns) do
    local pattern = _pattern[1]
    local fg_color = _pattern.fg or "default"
    local bg_color = _pattern.bg or "default"
    local current_line_data = {}

    for i=1,#line_data do
      line_item = line_data[i]
      if type(line_item) == "string" then
        for nonmatch,match in rex_pcre.split(line_item, pattern) do
          table.insert(current_line_data, nonmatch)
          if match then
            table.insert(current_line_data,
              function() return FG_COLORS[fg_color], BG_COLORS[bg_color] end
            )
            table.insert(current_line_data, match)
            table.insert(current_line_data, function() end)
          end
        end
      else
        table.insert(current_line_data, line_item)
      end
    end

    line_data = current_line_data
  end

  local fg_color_stack, bg_color_stack = { FG_COLORS.default }, { BG_COLORS.default }
  local result = ""
  local fg_color, bg_color
  for i=1,#line_data do
    line_item = line_data[i]
    if type(line_item) == "function" then
      fg_color, bg_color = line_item()
      if fg_color then
        table.insert(fg_color_stack, fg_color)
        table.insert(bg_color_stack, bg_color)
        result = result .. fg_color .. bg_color
      else
        table.remove(fg_color_stack)
        table.remove(bg_color_stack)
        result = result .. RESET_COLORS
                        .. fg_color_stack[#fg_color_stack]
                        .. bg_color_stack[#bg_color_stack]
      end
    elseif type(line_item) == "string" then
      result = result .. line_item
    end
  end

  return result
end


local function split_colors(tbl, colors)
  local split = rex_pcre.split(colors, ":")
  tbl.fg = split() or "auto"
  tbl.bg = split() or "default"
  return tbl
end

local function err_warn(fmt, ...)
  io.stderr:write(fmt:format(...)..'\n')
end

local function err_exit(code, fmt, ...)
  err_warn(fmt, ...)
  os.exit(code)
end


require "os"

local options = { patterns_file = os.getenv("YUPI_PATTERNS") or "default.yupi" }
local wait_for = ""
for i=1,#arg do
  local arg0 = arg[i]
  if wait_for ~= "" then
    options[wait_for] = arg0
  
  elseif arg0 == "-p" then
    wait_for = "patterns_file"
  elseif arg0 == "-h" or arg0 == "--help" then
    err_warn(USAGE)
    os.exit(0)
  else
    err_exit(1, "Unrecognized command line option '%s'. (Try --help)", arg0)
  end
end

local patterns_file, err = io.open(options.patterns_file, "r")
if not patterns_file then
  err_exit(2, "Error opening file: %s", err)
end

local patterns = {}
for line in patterns_file:lines() do
  if line[1] ~= "#" then
    local split = rex_pcre.split(line, "\\s+")
    local regex = split()
    local colors = split()
    if regex then
      table.insert(patterns, split_colors({ regex }, colors))
    else -- TODO FIXME make print warning if verbose etc
    end
  end
end

for line in io.lines() do
  print(FG_COLORS.default..BG_COLORS.default..colorize_line({ line }, patterns))
end

io.write(RESET_COLORS)

